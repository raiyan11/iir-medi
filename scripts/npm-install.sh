#!/bin/bash
cd /home/ubuntu/frontend
npm install
ng build --env=staging -sm --vendor-chunk=false --output-hashing=none
for f in ./dist/*.js;do uglifyjs $f -o $f --compress --mangle  ;done
gzip -r -9 dist/
while IFS= read -r file; do mv $file ${file%.gz}; done < <(find dist/ -type f -name "*.gz")
