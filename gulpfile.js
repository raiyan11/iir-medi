const gulp = require('gulp');
const runSequence = require('run-sequence');
const uglify = require('gulp-uglify');
const pump = require('pump');
const child = require('child_process');
const gzip = require('gulp-gzip');
const cssnano = require('gulp-cssnano');
const sourcemaps = require('gulp-sourcemaps');
const slack = require('gulp-slack')({
  url: 'https://hooks.slack.com/services/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',
  icon_emoji: ":+1:",
  channel: '#iir',
  user: 'Angular'
});
const argv = require('yargs')
  .option('env', {
    demand: true,
    alias: 'environment',
    describe: 'Environment for angular build',
    choices: ['dev', 'staging', 'prod']
  })
  .argv;

const server_address = function () {
  switch (argv.env) {
    case 'staging': {
      return 'ubuntu@iir.bitcot.com'
    }
    case 'prod': {
      return 'ubuntu@iir.bitcot.com'
    }
    default:
      throw Error('use `ng s` for local.');
  }
};

gulp.task('clean', (callback) => {
  child.exec('rm -rf dist/', (e, sin, sout) => {
    callback(e);
  });
});

gulp.task('version_bump', (callback) => {
  const bumpType = (argv.env === 'prod') ? 'major' : 'patch';
  child.exec(`npm version ${bumpType} --no-git-tag-version`, (e, sin, sout) => callback(e));
});

gulp.task('dist-browser', (callback) => {
  child.spawn('ng', ['build', '--prod', `--env=${argv.env}`, '-sm', '--vendor-chunk=false', '--output-hashing=none', '--buildOptimizer', '--aot'], {stdio: 'inherit'})
    .on('close', callback);
});

gulp.task('compress', function (callback) {
  pump([
    gulp.src('dist/**'),
    gzip({append: true}),
    gulp.dest('dist/')
  ], callback);
});

gulp.task('minify-js', (callback) => {
  pump([
    gulp.src('dist/*.js'),
    uglify(),
    gulp.dest('dist'),
  ], callback);
});

gulp.task('minify-css', function (callback) {
  pump([
    gulp.src('dist/**.css'),
    sourcemaps.init(),
    cssnano(),
    sourcemaps.write('.'),
    gulp.dest('dist')
  ], callback);
});

gulp.task('deploy', (callback) => {
  const server = server_address();
  child.spawn('scp', ['-r', 'dist/', `${server}:/home/ubuntu`], {stdio: 'inherit'})
    .on('close', callback);
});

gulp.task('build', (callback) => {
  runSequence('clean', 'version_bump', 'dist-browser', 'minify-js', 'minify-css', 'compress', 'deploy',
    function (err) {
      if (err) {
        console.log("Error!");
        slack(`Deployment failed`);
      } else {
        console.log('success!');
        child.exec('git rev-parse --abbrev-ref  HEAD', (e, sin, sout) => {
          const branch = sin.trim();
          const appVersion = require('./package').version;
          slack(`Deployed branch ${branch} to ${argv.env}. v${appVersion}`);
        });
      }
    }
  )
});

