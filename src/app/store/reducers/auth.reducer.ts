import * as fromAuth from '../actions';

export interface AuthState {
  user: any;
  loggedIn: boolean;
  loggedOut: boolean;
  error: string;
  isOktaUser: boolean;
}

export const initialState = {
  user: null,
  loggedIn: false,
  loggedOut: true,
  error: null,
  isOktaUser: false
};

export function reducer(state = initialState, action: fromAuth.AuthAction): AuthState {
  switch (action.type) {
    case fromAuth.LOAD_USER: {
      return {
        ...state,
        loggedIn: true,
        loggedOut: false,
        user: action.payload
      };
    }
    case fromAuth.LOGIN_USER_SUCCESS: {
      return {
        ...state,
        loggedIn: true,
        loggedOut: false,
        user: action.payload,
        error: null
      };
    }
    case fromAuth.LOGIN_USER: {
      return {
        ...state,
        loggedIn: false,
        loggedOut: true,
        error: null
      };
    }
    case fromAuth.LOGIN_USER_FAIL: {
      return {
        ...state,
        loggedIn: false,
        loggedOut: true,
        user: null,
        error: action.payload
      };
    }
    case fromAuth.NOT_AUTHENTICATED: {
      return {
        ...state,
        loggedIn: false,
        loggedOut: true,
        user: null,
        error: null
      };
    }
    case fromAuth.LOGOUT_USER: {
      return {
        ...state,
        loggedIn: false,
        loggedOut: true,
        user: null,
        error: null
      };
    }
    case fromAuth.OKTA_LOGIN: {
      return {
        ...state,
        isOktaUser: false,
        error: null,
        loggedIn: false,
        loggedOut: true,
      };
    }
    case fromAuth.OKTA_LOGIN_SUCCESS: {
      return {
        ...state,
        isOktaUser: true,
        user: action.payload,
        loggedIn: true,
        loggedOut: false,
        error: null,
      };
    }
    case fromAuth.OKTA_LOGIN_ERROR: {
      return {
        ...state,
        isOktaUser: false,
        loggedIn: false,
        loggedOut: true,
        error: action.payload,
      };
    }
    case fromAuth.FORGOT_PASSWORD_SUCCESS: {
      return {
        ...state,
        error: action.payload,
      };
    }
  }
  return state;
}

