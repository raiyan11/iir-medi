import {ActionReducer, ActionReducerMap, MetaReducer} from '@ngrx/store';
import * as fromAuth from './auth.reducer';
import * as fromAuthAction from '../actions/auth.action';
import {InjectionToken} from '@angular/core';
import * as fromApplications from '../../applications/store';

export interface AppState {
  auth: fromAuth.AuthState;
  applications: fromApplications.ApplicationsState;
}

const reducers = {
  auth: fromAuth.reducer,
  applications: fromApplications.reducer,
};

export const reducerToken = new InjectionToken<ActionReducerMap<AppState>>('Reducer Registered');
export const reducerProvider = [{provide: reducerToken, useValue: reducers}];


export const metaReducers: MetaReducer<any>[] = [rootReducer];

export function rootReducer(reducer: ActionReducer<any>): ActionReducer<any> {
  return function (state, action) {
    switch (action.type) {
      case fromAuthAction.LOGOUT_USER:
        state = undefined;
        break;
      case fromApplications.RESET_PROJECT:
        state.applications = undefined;
        break;
    }
    return reducer(state, action);
  };
}
