import {Injectable} from '@angular/core';
import {Effect, Actions} from '@ngrx/effects';
import * as authAction from '../actions/auth.action';
import {map, switchMap} from 'rxjs/operators';
import {of} from 'rxjs/observable/of';
import {Action} from '@ngrx/store';
import {Observable} from 'rxjs/Observable';
import {AuthService} from '../../shared/services/auth.service';
import {OktaAuthService} from '../../shared/services/okta-auth.service';


@Injectable()
export class AuthEffect {
  constructor(private actions: Actions, private auth: AuthService, private oktaAuth: OktaAuthService) {
  }

  @Effect()
  loginUser: Observable<Action> = this.actions.ofType(authAction.LOGIN_USER)
    .map((action: authAction.LoginUser) => action.payload)
    .pipe(
      switchMap((user) => {
        return this.auth.signIn(user)
          .map(_user => new authAction.LoginUserSuccess(_user))
          .catch(error => of(new authAction.LoginUserFail(error)));
      }));


  @Effect({dispatch: false})
  logoutUser: Observable<Action> = this.actions.ofType(authAction.LOGOUT_USER)
    .map(() => {
      this.oktaAuth.logout();
      return new authAction.LogoutUser();
    });

  @Effect()
  oktaUserLogin: Observable<Action> = this.actions.ofType(authAction.OKTA_LOGIN)
    .pipe(
      switchMap(async () => {
        const session = await this.oktaAuth.getSession();
        if (session) {
          const user = await session.user();
          localStorage.setItem('session_id', session.id);
          localStorage.setItem('okta_user', JSON.stringify(user));
          return new authAction.OktaLoginSuccess(user);
        } else {
          return new authAction.OktaLoginFail('Authentication failed');
        }
      }));

}

