import {AuthEffect} from './auth.effect';
import {ApplicationsEffects} from '../../applications/store/effects';

export const RootEffects = [
  AuthEffect,
  ...ApplicationsEffects,
];
