import {Action} from '@ngrx/store';

export const LOAD_USER = '[Auth] Load User';
export const LOGIN_USER = '[Auth] Login User';
export const LOGOUT_USER = '[Auth] Logout User';
export const LOGIN_USER_SUCCESS = '[Auth] Login User Success';
export const LOGIN_USER_FAIL = '[Auth] Login User Fail';
export const NOT_AUTHENTICATED = '[Auth] Not Authenticated';

export const FORGOT_PASSWORD = '[Auth] Forgot Password';
export const FORGOT_PASSWORD_SUCCESS = '[Auth] Forgot Password Success';

export const OKTA_LOGIN = '[Auth] Okta Login';
export const OKTA_LOGIN_SUCCESS = '[Auth] Okta Login Success';
export const OKTA_LOGIN_ERROR = '[Auth] Okta Login Error';


export class LoginUser implements Action {
  readonly type = LOGIN_USER;

  constructor(public payload: any) {
  }
}

export class LoadUser implements Action {
  readonly type = LOAD_USER;

  constructor(public payload: any) {
  }
}

export class LoginUserFail implements Action {
  readonly type = LOGIN_USER_FAIL;

  constructor(public payload: any) {
  }
}

export class OktaLogin implements Action {
  readonly type = OKTA_LOGIN;
}

export class OktaLoginSuccess implements Action {
  readonly type = OKTA_LOGIN_SUCCESS;

  constructor(public payload: any) {
  }
}

export class OktaLoginFail implements Action {
  readonly type = OKTA_LOGIN_ERROR;

  constructor(public payload: any) {
  }
}

export class LoginUserSuccess implements Action {
  readonly type = LOGIN_USER_SUCCESS;

  constructor(public payload: any) {
  }
}

export class UserNotAuthenticated implements Action {
  readonly type = NOT_AUTHENTICATED;
}

export class ForgotPassword implements Action {
  readonly type = FORGOT_PASSWORD;

  constructor(public payload: any) {
  }
}

export class ForgotPasswordSuccess implements Action {
  readonly type = FORGOT_PASSWORD_SUCCESS;

  constructor(public payload: any) {
  }
}

export class LogoutUser implements Action {
  readonly type = LOGOUT_USER;
}

export type AuthAction =
  LoadUser
  | LoginUser
  | LoginUserSuccess
  | LoginUserFail
  | UserNotAuthenticated
  | OktaLogin
  | OktaLoginSuccess
  | OktaLoginFail
  | ForgotPassword
  | ForgotPasswordSuccess
  | LogoutUser;
