import {BrowserModule} from '@angular/platform-browser';
import {NgModule, PLATFORM_ID} from '@angular/core';
import {AppComponent} from './app.component';
import {AppRoutingModule} from './app-routing.module';
import {CommonModule, isPlatformBrowser} from '@angular/common';
import {reducerToken, metaReducers, reducerProvider, RootEffects} from './store';
import {EffectsModule} from '@ngrx/effects';
import {RouterStateSerializer, StoreRouterConnectingModule} from '@ngrx/router-store';
import {StoreModule} from '@ngrx/store';
import {StoreDevtoolsModule} from '@ngrx/store-devtools';
import {HttpClientModule} from '@angular/common/http';
import {environment} from '../environments/environment';
import {CustomSerializer} from './store/reducers/router.reducer';
import {JwtModule} from '@auth0/angular-jwt';
import {AppConstants} from './shared/app.constants';
import {ToastrModule} from 'ngx-toastr';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {CoreModuleModule} from './shared/core-module/core-module.module';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,
    CommonModule,
    CoreModuleModule,
    EffectsModule.forRoot(RootEffects),
    HttpClientModule,
    ToastrModule.forRoot(),
    JwtModule.forRoot({
      config: {
        tokenGetter: tokenGetter,
      }
    }),
    StoreModule.forRoot(reducerToken, {metaReducers}),
    StoreRouterConnectingModule,
    StoreDevtoolsModule.instrument({
      maxAge: 25, // Retains last 25 states
      logOnly: environment.production, // Restrict extension to log-only mode
    }),
  ],
  providers: [AppConstants, {
    provide: RouterStateSerializer,
    useClass: CustomSerializer
  }, reducerProvider],
  bootstrap: [AppComponent]
})
export class AppModule {
}

export function tokenGetter() {
  if (isPlatformBrowser(PLATFORM_ID)) {
    return localStorage.getItem('token');
  }
  return '';
}
