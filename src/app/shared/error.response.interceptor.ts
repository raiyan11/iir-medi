import {
  HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest,
  HttpResponse
} from '@angular/common/http';
import {Injectable, Injector} from '@angular/core';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class ErrorResponseInterceptor implements HttpInterceptor {
  constructor() {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler) {
    const authReq = req.clone({headers: req.headers.set('session_id', localStorage.getItem('session_id'))});
    return next.handle(authReq).map((event: HttpEvent<any>) => {
      if (event instanceof HttpResponse) {
        if (event.body.success === 'false' || event.body.success === false) {
          // throw custom json error block.
          throw new HttpErrorResponse({
            error: event.body.error,
            status: event.status,
            statusText: 'Something went wrong'
          });
        }
      }
      return event;
    }).catch(event => {
      // other errors are HttpErrorResponse handles by AppConstants.
      if (event instanceof HttpErrorResponse) {
        if (event.error.success === 'false' || event.error.success === false) {
          if (event.status === 403) {
            localStorage.clear();
            window.location.href = '/login';
          }
        }
      }
      return Observable.throw(event);
    });
  }
}
