import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import * as OktaAuth from '@okta/okta-auth-js';
import {environment} from '../../../environments/environment';
import {AppConstants} from '../app.constants';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';


@Injectable()
export class OktaAuthService {

  oktaAuth = new OktaAuth(environment.oktaConfig);
  public currentUser = new BehaviorSubject<any>(this.getCurrentUser());

  constructor(private router: Router, private appConstants: AppConstants) {
  }

  async isAuthenticated() {
    // Checks if there is a current accessToken in the TokenManger.
    return await this.oktaAuth.session.exists();
  }

  private getCurrentUser() {
    return this.isAuthenticated() ? JSON.parse(localStorage.getItem('okta_user')) : null;
  }

  login() {
    // Launches the login redirect.
    this.oktaAuth.token.getWithRedirect({
      responseType: ['id_token', 'token'],
      scopes: ['openid', 'email', 'profile']
    });
  }

  async loginWithEmail(userObj) {
    return await this.oktaAuth.signIn(userObj);
  }

  async handleAuthentication() {
    try {
      const tokens = await this.oktaAuth.token.parseFromUrl();
      tokens.forEach(token => {
        if (token.idToken) {
          this.oktaAuth.tokenManager.add('idToken', token);
        }
        if (token.accessToken) {
          this.oktaAuth.tokenManager.add('accessToken', token);
        }
      });
      return tokens;
    } catch {
      return null;
    }

  }

  async setSessionCookie(session) {
    this.oktaAuth.token.getWithoutPrompt({
      sessionToken: session.sessionToken,
      responseType: ['id_token', 'token'],
    }).then(tokens => {
      tokens.forEach(token => {
        if (token.idToken) {
          this.oktaAuth.tokenManager.add('idToken', token);
        }
        if (token.accessToken) {
          this.oktaAuth.tokenManager.add('accessToken', token);
        }
      });
      this.oktaAuth.session.setCookieAndRedirect(session.sessionToken, environment.oktaConfig.redirectUri);
    });
  }

  async getSession() {
    return await this.oktaAuth.session.get();
  }

  async getSessionUser() {
    const accessToken = await this.oktaAuth.tokenManager.get('accessToken');
    return this.oktaAuth.token.getUserInfo(accessToken);
  }

  async logout() {
    await this.oktaAuth.signOut();
    this.oktaAuth.tokenManager.clear();
    localStorage.clear();
    window.location.href = '/login';
  }
}
