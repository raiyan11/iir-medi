import {JwtHelperService} from '@auth0/angular-jwt';
import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {isNullOrUndefined} from 'util';
import {environment} from '../../../environments/environment';
import {Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';
import {Router} from '@angular/router';

@Injectable()
export class AuthService {

  constructor(private  jwtHelper: JwtHelperService, private http: HttpClient, private router: Router) {
  }

  public currentUser = new BehaviorSubject<any>(this.getUserData());

  loggedIn() {
    const token = this.getAuthorizationToken();
    return !isNullOrUndefined(this.jwtHelper.tokenGetter()) && !this.jwtHelper.isTokenExpired(token);
  }

  getHeaders() {
    return new HttpHeaders().set('Accept', 'application/json').set('Content-Type', 'application/json');
  }

  getAuthorizationHeaders() {
    return this.getHeaders().set('Authorization', 'JWT ' + this.getAuthorizationToken());
  }

  getAuthorizationToken() {
    return this.jwtHelper.tokenGetter();
  }

  setAuthorizationToken(token) {
    localStorage.setItem('token', token);
  }

  deleteAuthorizationToken() {
    localStorage.removeItem('token');
  }

  setUserData(user) {
    localStorage.setItem('user', JSON.stringify(user));
  }

  deleteUserData() {
    localStorage.removeItem('user');
  }

  private getUserData() {
    return JSON.parse(localStorage.getItem('user'));
  }

  signIn(user): any {
    return this.http.post(environment.loginUrl, user, {headers: this.getHeaders()})
      .map(response => {
        const _user = response['user'];
        _user.token = response['token'];
        if (!isNullOrUndefined(_user) && !this.jwtHelper.isTokenExpired(_user.token)) {
          this.setAuthorizationToken(_user.token);
          this.setUserData(_user);
          this.currentUser.next(_user);
        }
        return _user;
      })
      .catch(this.handleError);
  }

  signOut() {
    this.deleteAuthorizationToken();
    this.deleteUserData();
    this.router.navigate(['/login']);
  }

  private handleError(errorRes: HttpErrorResponse | any) {
    let errorMsg: string;
    if (errorRes instanceof HttpErrorResponse) {
      errorMsg = errorRes.error;
    } else {
      errorMsg = 'Something went wrong!';
    }
    return Observable.throw(errorMsg);
  }

}

