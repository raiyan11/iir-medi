import {Store} from '@ngrx/store';
import * as fromStore from '../store';
import {OktaAuthService} from './services/okta-auth.service';
import {Injector} from '@angular/core';

export function UserLoadInjector(store: Store<fromStore.AppState>): Function {
  return (): Promise<any> => {
    const injector = Injector.create({providers: [{provide: OktaAuthService, deps: []}]});
    const oktaAuth = injector.get(OktaAuthService);
    return new Promise((resolve, reject) => {
      const user = localStorage.getItem('okta_user');
      if (oktaAuth.isAuthenticated() && !!user) {
        store.dispatch(new fromStore.LoadUser(JSON.parse(user)));
      }
      resolve();
    });
  };
}
