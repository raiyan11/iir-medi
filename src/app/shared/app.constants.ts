import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Response} from '@angular/http';
import {HttpErrorResponse, HttpHeaders} from '@angular/common/http';

@Injectable()
export class AppConstants {

  errorHandler = function handleError(errorRes: HttpErrorResponse | any) {
    let errorMsg: string;
    if (errorRes instanceof HttpErrorResponse) {
      errorMsg = errorRes.error;
    } else {
      errorMsg = 'Something went wrong!';
    }
    return Observable.throw(errorMsg);
  };

  JSONHeaders(){
    return new HttpHeaders().append('Content-Type', 'application/json');
  }
}
