import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ClinicalSiteService} from '../../applications/services/clinical-site.service';
import {ProjectService} from '../../applications/services/project.service';
import {BudgetService} from '../../applications/services/budget.service';
import {AddlInfoService} from '../../applications/services/addl-info.service';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [],
  providers: [
    AddlInfoService,
    BudgetService,
    ClinicalSiteService,
    ProjectService
  ]
})
export class CoreModuleModule {
}
