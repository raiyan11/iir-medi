import {Component, OnDestroy, OnInit} from '@angular/core';
import {OktaAuthService} from '../shared/services/okta-auth.service';
import {Store} from '@ngrx/store';
import * as fromStore from '../store';
import {Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {
  loginStore;
  formSubmit = false;
  userLogin = {
    username: null,
    password: null,
  };

  constructor(private oktaAuth: OktaAuthService,
              private store: Store<fromStore.AppState>,
              private router: Router,
              private toastr: ToastrService) {
  }

  async ngOnInit() {
  }

  ngOnDestroy() {
    // this.loginStore.unsubscribe();
  }

  async loginUser() {
    this.formSubmit = true;
    try {
      const user = await this.oktaAuth.loginWithEmail(this.userLogin);
      if (user.status === 'SUCCESS') {
        this.oktaAuth.setSessionCookie(user);
      } else {
        this.formSubmit = false;
        this.toastr.error('Something went wrong!', 'Error');
      }
    } catch (e) {
      this.formSubmit = false;
      this.toastr.error(e.message || 'Please check your credentials', 'Error');
    }
  }

  oktaLogin() {
    this.oktaAuth.login();
  }

}
