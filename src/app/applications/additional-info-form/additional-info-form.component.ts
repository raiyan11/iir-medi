import {Component, EventEmitter, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import {AddlInfo, AddlInfoDocumentTypes} from '../../models/addl-info';
import * as fromStore from '../../store';
import {Store} from '@ngrx/store';
import {ToastrService} from 'ngx-toastr';
import {Project} from '../../models/project';
import {take} from 'rxjs/operators';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-additional-info-form',
  templateUrl: './additional-info-form.component.html',
  styleUrls: ['./additional-info-form.component.scss']
})
export class AdditionalInfoFormComponent implements OnInit, OnDestroy {
  @Output() onNextClick = new EventEmitter<any>();
  @Output() onPrevClick = new EventEmitter<any>();
  @ViewChild('addlInfoForm') addlInfoForm: NgForm;
  projectStore;
  addlInfoStore;
  project: Project;
  addlInfo = new AddlInfo();
  addlDocuments = AddlInfoDocumentTypes;
  submitted;

  constructor(private store: Store<fromStore.AppState>,
              private toastr: ToastrService) {
  }

  ngOnInit() {
    this.submitted = false;
    this.projectStore = this.store.select(s => s.applications.project).subscribe(project => {
      this.project = project.data;
    });
    this.store.select(s => s.applications.addl_info).pipe(take(1)).subscribe(r => {
      if (r.data) {
        this.addlInfo.documents = r.data;
      }
    });
  }

  ngOnDestroy() {
    if (this.addlInfoStore) {
      this.addlInfoStore.unsubscribe();
    }
    this.projectStore.unsubscribe();
  }

  saveDraft() {
    this.addlInfo.draft = true;
    this.storeDispatch();
  }

  formSubmit($event) {
    this.addlInfo.draft = false;
    this.storeDispatch($event);
  }

  storeDispatch($event = null) {
    this.submitted = true;
    this.store.dispatch(new fromStore.CreateAddlInfo({body: this.addlInfo, projectId: this.project.id}));
    this.store.select(s => s.applications.addl_info).pipe(take(2)).subscribe(r => {
      this.toastr.clear();
      if (r.error) {
        this.submitted = false;
        this.toastr.error(r.error, 'Error');
        return;
      }
      if (r.saving) {
        this.toastr.info('Saving...');
      }
      if (r.saved) {
        if ($event) {
          this.toastr.success('Saved');
          this.onNextClick.emit($event);
        } else {
          this.submitted = false;
          this.toastr.success('Saved as Draft');
        }
      }
    });
  }

  onDocumentChange($event, type) {
    if ($event.target && $event.target.files.length > 0) {
      this.addlInfo[type] = $event.target.files[0];
      if (this.addlInfo.documents) {
        // remove documents for the category.
        this.addlInfo.documents = this.addlInfo.documents.filter(d => d.category !== type);
      }
    }
    this.addlInfoForm.controls[type].updateValueAndValidity();
  }

  nextClick($event) {
    this.onNextClick.emit($event);
  }

  prevClick($event) {
    this.onPrevClick.emit($event);
  }

  documentForCategory(category) {
    return this.addlInfo.documents ? this.addlInfo.documents.filter(d => d.category === category)[0] : null;
  }

}
