import {Component, OnInit} from '@angular/core';

declare const $: any;

@Component({
  selector: 'app-confirmation',
  templateUrl: './confirmation.component.html',
  styleUrls: ['./confirmation.component.scss']
})
export class ConfirmationComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {
    $('.f1-steps').find('.f1-step').last().addClass('activated').removeClass('active');
  }

}
