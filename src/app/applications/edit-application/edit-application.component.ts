import {Component, OnDestroy, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import * as fromStore from '../../store';
import {ActivatedRoute} from '@angular/router';
import {ProjectStageType} from '../../models/project';
import {take} from 'rxjs/operators';

declare var $: any;

@Component({
  selector: 'app-edit-application',
  templateUrl: './edit-application.component.html',
  styleUrls: ['./edit-application.component.scss']
})
export class EditApplicationComponent implements OnInit, OnDestroy {
  currentTab;
  project;
  currentProgressValue = 0;

  constructor(private store: Store<fromStore.AppState>,
              private route: ActivatedRoute) {
  }

  ngOnDestroy() {
    this.store.dispatch(new fromStore.ResetProjectForms());
  }

  ngOnInit() {
    this.store.dispatch(new fromStore.GetProjectOverview(this.route.snapshot.params.applicationId));
    this.project = this.store.select(s => s.applications.project);
    this.project.pipe(take(2)).subscribe(r => {
      if (r.error) {
        return;
      }
      if (r.loaded) {
        setTimeout(() => {
          const progress_line = $('html,body').find('.f1').find('.f1-progress-line');
          const current_active_step = $('html,body').find('.f1').find('.f1-step.active');
          const currentStage = r.data.stage;
          current_active_step.first().addClass('activated').removeClass('active');
          this.currentTab = +ProjectStageType[currentStage] + 1;

          if (+ProjectStageType[currentStage] === 0) {
            current_active_step.nextAll().first().addClass('active');
          } else {
            current_active_step.nextAll().slice(0, +ProjectStageType[currentStage]).addClass('activated').next().last().addClass('active');
          }
          this.currentProgressValue = +ProjectStageType[currentStage] * 20;
          this.bar_progress(progress_line, 'right');
        }, 200);
      }
    });
  }

  bar_progress(progress_line_object, direction) {
    const number_of_steps = progress_line_object.data('number-of-steps');
    const now_value = this.currentProgressValue; // progress_line_object.data('now-value');
    let new_value = 0;
    if (direction === 'right') {
      new_value = Math.floor(now_value + (100 / number_of_steps) + 4);
    }
    else if (direction === 'left') {
      new_value = Math.ceil(now_value - (100 / number_of_steps) - 4);
    }
    this.currentProgressValue = new_value;
    progress_line_object.attr('style', 'width: ' + new_value + '%;').data('now-value', new_value);
  }

  nextClick($event) {
    this.currentTab++;
    const $this = $event.target;
    const parent_fieldset = $($this).parents('fieldset');
    const next_step = true;
    // navigation steps / progress steps
    const current_active_step = $($this).parents('.f1').find('.f1-step.active');
    const progress_line = $($this).parents('.f1').find('.f1-progress-line');

    if (next_step) {
      parent_fieldset.fadeOut(400, () => {
        // change icons
        current_active_step.removeClass('active').addClass('activated').next().addClass('active');
        // progress bar
        this.bar_progress(progress_line, 'right');
        // show next step
        $($($this).parents()[1]).parent().parent().next().children().fadeIn();
        // scroll window to beginning of the form
        this.scroll_to_class($('.f1'), 20);
      });
    }
  }

  prevClick($event) {
    this.currentTab--;
    const $this = $event.currentTarget;
    const current_active_step = $($this).parents('.f1').find('.f1-step.active');
    const progress_line = $($this).parents('.f1').find('.f1-progress-line');

    $($this).parents('fieldset').fadeOut(400, () => {
      // change icons
      current_active_step.removeClass('active').prev().removeClass('activated').addClass('active');
      // progress bar
      this.bar_progress(progress_line, 'left');
      // show previous step
      $($($this).parents()[1]).parent().parent().prev().children().fadeIn();
      // scroll window to beginning of the form
      this.scroll_to_class($('.f1'), 20);
    });
  }


  scroll_to_class(element_class, removed_height) {
    const scroll_to = $(element_class).offset().top - removed_height;
    if ($(window).scrollTop() !== scroll_to) {
      $('html, body').stop().animate({scrollTop: scroll_to}, 0);
    }
  }

}
