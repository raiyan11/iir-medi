import {Component, EventEmitter, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import {FormBuilder, NgForm} from '@angular/forms';
import {Store} from '@ngrx/store';
import * as fromStore from '../../store';
import {ToastrService} from 'ngx-toastr';
import {Project} from '../../models/project';
import {take} from 'rxjs/operators';
import {isNullOrUndefined} from 'util';
import {Investigator} from '../../models/investigator';

@Component({
  selector: 'app-clinical-site-form',
  templateUrl: './clinical-site-form.component.html',
  styleUrls: ['./clinical-site-form.component.scss']
})
export class ClinicalSiteFormComponent implements OnInit, OnDestroy {
  @Output() onNextClick = new EventEmitter<any>();
  @Output() onPrevClick = new EventEmitter<any>();
  @ViewChild('clinicalForm') clinicalForm: NgForm;
  project: Project;
  investigator = new Investigator();
  projectStore;
  clinicalStore;
  submitted;

  constructor(private fb: FormBuilder,
              private store: Store<fromStore.AppState>,
              private toastr: ToastrService) {
  }

  ngOnInit() {
    this.submitted = false;
    this.projectStore = this.store.select(s => s.applications.project).subscribe(project => {
      this.project = project.data;
    });
    this.store.select(s => s.applications.clinical_site).pipe(take(1)).subscribe(r => {
      // or saved as draft.
      if ((r.saved || r.loaded) && (!isNullOrUndefined(r.data))) {
        this.investigator = r.data;
      }
    });
  }

  ngOnDestroy() {
    if (this.clinicalStore) {
      this.clinicalStore.unsubscribe();
    }
    this.projectStore.unsubscribe();
  }

  onDocumentChange($event) {
    if ($event.target.files && $event.target.files[0]) {
      this.investigator.file = $event.target.files[0];
      this.investigator.documents = [];
    }
  }

  formSubmit($event) {
    this.investigator.draft = false;
    this.storeDispatch($event);
  }

  storeDispatch($event = null) {
    this.submitted = true;
    this.store.dispatch(new fromStore.CreateInvestigator({body: this.investigator, projectId: this.project.id}));
    this.store.select(s => s.applications.clinical_site).pipe(take(2)).subscribe(r => {
      this.toastr.clear();
      if (r.error) {
        this.submitted = false;
        this.toastr.error(r.error, 'Error');
        return;
      }
      if (r.saving) {
        this.toastr.info('Saving...');
      }
      if (r.saved) {
        if ($event) {
          this.toastr.success('Saved');
          this.onNextClick.emit($event);
        } else {
          this.submitted = false;
          this.toastr.success('Saved as Draft');
        }
      }
    });

  }

  saveDraft() {
    this.investigator.draft = true;
    this.storeDispatch();
  }

  prevClick($event) {
    this.onPrevClick.emit($event);
  }
}
