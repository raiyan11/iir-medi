import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClinicalSiteFormComponent } from './clinical-site-form.component';

describe('ClinicalSiteFormComponent', () => {
  let component: ClinicalSiteFormComponent;
  let fixture: ComponentFixture<ClinicalSiteFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClinicalSiteFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClinicalSiteFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
