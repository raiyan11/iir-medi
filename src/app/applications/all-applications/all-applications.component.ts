import {Component, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import * as fromStore from '../../store';
import {ProjectStatusType} from '../../models/project';
import {Observable} from 'rxjs/Observable';
import {skip, take} from 'rxjs/operators';

@Component({
  selector: 'app-all-applications',
  templateUrl: './all-applications.component.html',
  styleUrls: ['./all-applications.component.scss']
})
export class AllApplicationsComponent implements OnInit {
  projects;
  newUser: boolean = false;
  filterBy = {
    status: 'all',
    name: null,
    title: null,
    appId: null
  };
  projectStatus = ProjectStatusType;
  loaded$: Observable<any>;
  filterStatuses;

  constructor(private store: Store<fromStore.AppState>) {
  }

  ngOnInit() {
    this.store.dispatch(new fromStore.GetAllProjects());
    this.loaded$ = this.store.select(s => s.applications.all_projects.loaded);
    this.store.select(s => s.applications.all_projects).pipe(take(2)).subscribe(r => {
      if (r.loading) {
        return;
      }
      if (r.loaded) {
        this.projects = r.data;
        this.newUser = r.data.length <= 0;
      }

    });
    this.filterStatuses = Object.values(ProjectStatusType).map(k => k);
  }

  filterData() {
    this.store.select(s => s.applications.all_projects.data).pipe(take(1)).subscribe(r => {
      let entities = r;
      if (this.filterBy.status) {
        entities = this.filterBy.status === 'all' ? r : r.filter(i => i.status === this.filterBy.status);
      }
      if (this.filterBy.name && this.filterBy.name.trim().length > 0) {
        entities = entities.filter(i => i.internalContact.toLowerCase().search(this.filterBy.name.toLowerCase()) > -1);
      }
      if (this.filterBy.title && this.filterBy.title.trim().length > 0) {
        entities = entities.filter(i => i.title.toLowerCase().search(this.filterBy.title.toLowerCase()) > -1);
      }
      if (this.filterBy.appId && this.filterBy.appId.trim().length > 0) {
        entities = entities.filter(i => i.applicationId.toLowerCase().search(this.filterBy.appId.toLowerCase()) > -1);
      }
      this.projects = entities;
    });
  }

  resetFilters() {
    this.filterBy = {
      status: 'all',
      name: null,
      title: null,
      appId: null
    };
    this.filterData();
  }

  withdrawProjectAction(projectId) {
    if (confirm('Do you want to withdraw the application?')) {
      this.store.dispatch(new fromStore.WithdrawAllProject(projectId));
      this.store.select(s => s.applications.all_projects).pipe(skip(1), take(1)).subscribe(r => {
        this.filterData();
      });
    }
  }

}
