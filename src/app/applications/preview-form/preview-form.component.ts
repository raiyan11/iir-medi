import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Store} from '@ngrx/store';
import * as fromStore from '../../store';
import {take} from 'rxjs/operators';
import {ToastrService} from 'ngx-toastr';
import {AddlInfoDocumentDetail} from '../../models/addl-info';


@Component({
  selector: 'app-preview-form',
  templateUrl: './preview-form.component.html',
  styleUrls: ['./preview-form.component.scss']
})
export class PreviewFormComponent implements OnInit {
  @Output() onNextClick = new EventEmitter<any>();
  @Output() onPrevClick = new EventEmitter<any>();
  application;
  projectId;

  constructor(private store: Store<fromStore.AppState>,
              private toastr: ToastrService) {
  }

  ngOnInit() {
    this.application = this.store.select(s => s.applications);
    this.store.select(s => s.applications.project.data).pipe(take(2)).subscribe(project => {
      if (project) {
        this.projectId = project.id;
      }
    });
  }

  confirmProject($event) {
    this.store.dispatch(new fromStore.SubmitProject(this.projectId));
    this.store.select(s => s.applications.project).pipe(take(2)).subscribe(r => {
      this.toastr.clear();
      if (r.error) {
        this.toastr.error(r.error, 'Error');
        return;
      }
      if (r.saving) {
        this.toastr.info('Saving...');
      }
      if (r.saved) {
        this.toastr.success('Saved');
        this.nextClick($event);
      }
    });

  }

  nextClick($event) {
    this.onNextClick.emit($event);
  }

  prevClick($event) {
    this.onPrevClick.emit($event);
  }

  addlInfoDocumentTitle(category) {
    return AddlInfoDocumentDetail[category];
  }


}
