import {Component, EventEmitter, OnDestroy, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {
  OtherEnrolmentGoalType,
  PatientPopulationType,
  PatientPopulationTypeDetail,
  ProjectAreaType,
  ProjectCategoryType
} from '../../models/project';
import {Store} from '@ngrx/store';
import * as fromStore from '../../store';
import {ToastrService} from 'ngx-toastr';
import {take} from 'rxjs/operators';
import {isNullOrUndefined} from 'util';

@Component({
  selector: 'app-project-form',
  templateUrl: './project-form.component.html',
  styleUrls: ['./project-form.component.scss']
})
export class ProjectFormComponent implements OnInit, OnDestroy {
  @Output() onNextClick = new EventEmitter<any>();
  projectForm: FormGroup;
  projectCategories;
  patientPopulationTypes;
  projectAreaType = ProjectAreaType;
  otherEnrolmentGoalTypes;
  submitted;
  private projectStore;

  constructor(private fb: FormBuilder,
              private toastr: ToastrService,
              private store: Store<fromStore.AppState>) {
    this.formSetup();
  }

  ngOnDestroy() {
    // this.projectStore.unsubscribe();
  }

  ngOnInit() {
    // form dropdown values init.
    this.submitted = false;
    this.projectCategories = Object.values(ProjectCategoryType).map(value => {
      return {key: value, value: value.split('_').join(' ')};
    });
    this.patientPopulationTypes = Object.values(PatientPopulationType).map(v => ({key: v, value: PatientPopulationTypeDetail[v]}));
    const goalTypesValues = ['1 - 10', '10 - 50', '50 - 100', '100 - 300', '300+'];
    this.otherEnrolmentGoalTypes = Object.values(OtherEnrolmentGoalType).map((v, index) => (
      {
        key: v,
        value: goalTypesValues[index]
      }));

    this.store.select(s => s.applications.project).pipe(take(2)).subscribe(r => {
      // or saved as draft.
      if ((r.saved || r.loaded) && (!isNullOrUndefined(r.data))) {
        this.projectForm.patchValue({
          title: r.data.title,
          objectives: r.data.objectives,
          studyDates: r.data.studyDates,
          hypothesis: r.data.hypothesis,
          intervention: r.data.intervention,
          outcomesMeasured: r.data.outcomesMeasured,
          internalContact: r.data.internalContact,
          phase: r.data.phase,
          area: r.data.area,
          patientPopulation: r.data.patientPopulation,
          otherEnrollmentGoal: r.data.otherEnrollmentGoal
        });
      }
    });
  }

  formSetup() {
    this.projectForm = this.fb.group({
      title: ['', Validators.compose([Validators.required, Validators.minLength(2)])],
      objectives: ['', Validators.required],
      studyDates: ['', Validators.required],
      hypothesis: ['', Validators.required],
      intervention: ['', Validators.required],
      outcomesMeasured: ['', Validators.required],
      internalContact: ['', Validators.required],
      phase: ['', Validators.required],
      area: ['', Validators.required],
      patientPopulation: ['', Validators.required],
      otherEnrollmentGoal: ['', Validators.required],
      draft: [false],
    });
  }

  saveDraft() {
    this.projectForm.controls.draft.setValue(true);
    this.storeDispatch();
  }

  formSubmit($event) {
    this.projectForm.controls.draft.setValue(false);
    this.storeDispatch($event);
  }

  storeDispatch($event = null) {
    this.submitted = true;
    this.store.select(s => s.applications.project.data).pipe(take(1)).subscribe(r => {
      if (isNullOrUndefined(r)) {
        this.store.dispatch(new fromStore.CreateProject(this.projectForm.value));
      } else {
        this.store.dispatch(new fromStore.UpdateProject({body: this.projectForm.value, projectId: r.id}));
      }
    });

    this.store.select(s => s.applications.project).pipe(take(2)).subscribe(r => {
      this.toastr.clear();
      if (!!r.error) {
        this.submitted = false;
        this.toastr.error(r.error, 'Error');
        return;
      }
      if (r.saving) {
        this.toastr.info('Saving...');
      }
      if (r.saved) {
        if ($event) {
          this.toastr.success('Saved');
          this.onNextClick.emit($event);
        } else {
          this.submitted = false;
          this.toastr.success('Saved as Draft');
        }
      }
    });
  }

}
