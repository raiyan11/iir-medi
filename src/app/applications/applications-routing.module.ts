import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {NewApplicationComponent} from './new-application/new-application.component';
import {AllApplicationsComponent} from './all-applications/all-applications.component';
import {ApplicationDetailComponent} from './application-detail/application-detail.component';
import {EditApplicationComponent} from './edit-application/edit-application.component';

const routes: Routes = [
  {path: '', component: AllApplicationsComponent},
  {path: 'new', component: NewApplicationComponent},
  {path: ':applicationId/overview', component: ApplicationDetailComponent},
  {path: ':applicationId/edit', component: EditApplicationComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ApplicationsRoutingModule {
}
