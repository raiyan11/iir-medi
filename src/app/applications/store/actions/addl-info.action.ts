import {Action} from '@ngrx/store';

export const CREATE_ADDL_INFO = '[ADDL_INFO] CREATE ';
export const CREATE_ADDL_INFO_SUCCESS = '[ADDL_INFO] CREATE Success';
export const CREATE_ADDL_INFO_FAIL = '[ADDL_INFO] CREATE Fail';

export const GET_ADDL_INFO = '[ADDL_INFO] GET ';
export const GET_ADDL_INFO_SUCCESS = '[ADDL_INFO] GET Success';
export const GET_ADDL_INFO_FAIL = '[ADDL_INFO] GET Fail';

export class CreateAddlInfo implements Action {
  readonly type = CREATE_ADDL_INFO;

  constructor(public payload: any) {
  }
}

export class CreateAddlInfoSuccess implements Action {
  readonly type = CREATE_ADDL_INFO_SUCCESS;

  constructor(public payload: any) {
  }
}

export class CreateAddlInfoFail implements Action {
  readonly type = CREATE_ADDL_INFO_FAIL;

  constructor(public payload: any) {
  }
}

export class GetAddlInfo implements Action {
  readonly type = GET_ADDL_INFO;

  constructor(public payload: any) {
  }
}

export class GetAddlInfoSuccess implements Action {
  readonly type = GET_ADDL_INFO_SUCCESS;

  constructor(public payload: any) {
  }
}

export class GetAddlInfoFail implements Action {
  readonly type = GET_ADDL_INFO_FAIL;

  constructor(public payload: any) {
  }
}

export type AddlInfoAction =
  CreateAddlInfo
  | CreateAddlInfoSuccess
  | CreateAddlInfoFail
  | GetAddlInfo
  | GetAddlInfoSuccess
  | GetAddlInfoFail;
