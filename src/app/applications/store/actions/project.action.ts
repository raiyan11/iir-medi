import {Action} from '@ngrx/store';

export const CREATE_PROJECT = '[PROJECT] CREATE ';
export const CREATE_PROJECT_SUCCESS = '[PROJECT] CREATE Success';
export const CREATE_PROJECT_FAIL = '[PROJECT] CREATE Fail';

export const UPDATE_PROJECT = '[PROJECT] UPDATE ';
export const UPDATE_PROJECT_SUCCESS = '[PROJECT] UPDATE Success';
export const UPDATE_PROJECT_FAIL = '[PROJECT] UPDATE Fail';

export const SUBMIT_PROJECT = '[PROJECT] SUBMIT ';
export const SUBMIT_PROJECT_SUCCESS = '[PROJECT] SUBMIT Success';
export const SUBMIT_PROJECT_FAIL = '[PROJECT] SUBMIT Fail';

export const GET_PROJECT_OVERVIEW = '[PROJECT] GET_PROJECT_OVERVIEW ';
export const GET_PROJECT_OVERVIEW_SUCCESS = '[PROJECT] GET_PROJECT_OVERVIEW Success';
export const GET_PROJECT_OVERVIEW_FAIL = '[PROJECT] GET_PROJECT_OVERVIEW Fail';

export const RESET_PROJECT = '[PROJECT] RESET_FORMS ';

// export const WITHDRAW_PROJECT = '[PROJECT] WITHDRAW ';
// export const WITHDRAW_PROJECT_SUCCESS = '[PROJECT] WITHDRAW Success';
// export const WITHDRAW_PROJECT_FAIL = '[PROJECT] WITHDRAW Fail';

export class CreateProject implements Action {
  readonly type = CREATE_PROJECT;

  constructor(public payload: any) {
  }
}

export class CreateProjectSuccess implements Action {
  readonly type = CREATE_PROJECT_SUCCESS;

  constructor(public payload: any) {
  }
}

export class CreateProjectFail implements Action {
  readonly type = CREATE_PROJECT_FAIL;

  constructor(public payload: any) {
  }
}

export class SubmitProject implements Action {
  readonly type = SUBMIT_PROJECT;

  constructor(public payload: any) {
  }
}

export class SubmitProjectSuccess implements Action {
  readonly type = SUBMIT_PROJECT_SUCCESS;
}

export class SubmitProjectFail implements Action {
  readonly type = SUBMIT_PROJECT_FAIL;

  constructor(public payload: any) {
  }
}

export class GetProjectOverview implements Action {
  readonly type = GET_PROJECT_OVERVIEW;

  constructor(public payload: any) {
  }
}

export class GetProjectOverviewSuccess implements Action {
  readonly type = GET_PROJECT_OVERVIEW_SUCCESS;

  constructor(public payload: any) {
  }
}

export class GetProjectOverviewFail implements Action {
  readonly type = GET_PROJECT_OVERVIEW_FAIL;

  constructor(public payload: any) {
  }
}

export class UpdateProject implements Action {
  readonly type = UPDATE_PROJECT;

  constructor(public payload: any) {
  }
}

export class UpdateProjectSuccess implements Action {
  readonly type = UPDATE_PROJECT_SUCCESS;

  constructor(public payload: any) {
  }
}

export class UpdateProjectFail implements Action {
  readonly type = UPDATE_PROJECT_FAIL;

  constructor(public payload: any) {
  }
}

export class ResetProjectForms implements Action {
  readonly type = RESET_PROJECT;

}

export type ProjectAction =
  CreateProject
  | CreateProjectSuccess
  | CreateProjectFail
  | SubmitProject
  | SubmitProjectSuccess
  | SubmitProjectFail
  | GetProjectOverview
  | GetProjectOverviewSuccess
  | GetProjectOverviewFail
  | UpdateProject
  | UpdateProjectSuccess
  | UpdateProjectFail
  | ResetProjectForms;
