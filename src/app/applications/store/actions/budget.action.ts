import {Action} from '@ngrx/store';

export const CREATE_BUDGET = '[BUDGET] CREATE ';
export const CREATE_BUDGET_SUCCESS = '[BUDGET] CREATE Success';
export const CREATE_BUDGET_FAIL = '[BUDGET] CREATE Fail';

export const GET_BUDGET = '[BUDGET] GET ';
export const GET_BUDGET_SUCCESS = '[BUDGET] GET Success';
export const GET_BUDGET_FAIL = '[BUDGET] GET Fail';

export class CreateBudget implements Action {
  readonly type = CREATE_BUDGET;

  constructor(public payload: any) {
  }
}

export class CreateBudgetSuccess implements Action {
  readonly type = CREATE_BUDGET_SUCCESS;

  constructor(public payload: any) {
  }
}

export class CreateBudgetFail implements Action {
  readonly type = CREATE_BUDGET_FAIL;

  constructor(public payload: any) {
  }
}

export class GetBudget implements Action {
  readonly type = GET_BUDGET;

  constructor(public payload: any) {
  }
}

export class GetBudgetSuccess implements Action {
  readonly type = GET_BUDGET_SUCCESS;

  constructor(public payload: any) {
  }
}

export class GetBudgetFail implements Action {
  readonly type = GET_BUDGET_FAIL;

  constructor(public payload: any) {
  }
}

export type BudgetAction =
  CreateBudget
  | CreateBudgetSuccess
  | CreateBudgetFail
  | GetBudget
  | GetBudgetSuccess
  | GetBudgetFail;
