import {Action} from '@ngrx/store';
export const GET_ALL_PROJECTS = '[PROJECT] GET_ALL ';
export const GET_ALL_PROJECTS_SUCCESS = '[PROJECT] GET_ALL Success';
export const GET_ALL_PROJECTS_FAIL = '[PROJECT] GET_ALL Fail';

export const WITHDRAW_ALL_PROJECT = '[ALL_PROJECTS] WITHDRAW ';
export const WITHDRAW_ALL_PROJECT_SUCCESS = '[ALL_PROJECTS] WITHDRAW Success';
export const WITHDRAW_ALL_PROJECT_FAIL = '[ALL_PROJECTS] WITHDRAW Fail';

export class GetAllProjects implements Action {
  readonly type = GET_ALL_PROJECTS;

}

export class GetAllProjectsSuccess implements Action {
  readonly type = GET_ALL_PROJECTS_SUCCESS;

  constructor(public payload: any) {
  }
}

export class GetAllProjectsFail implements Action {
  readonly type = GET_ALL_PROJECTS_FAIL;

  constructor(public payload: any) {
  }
}


export class WithdrawAllProject implements Action {
  readonly type = WITHDRAW_ALL_PROJECT;

  constructor(public payload: any) {
  }
}

export class WithdrawAllProjectSuccess implements Action {
  readonly type = WITHDRAW_ALL_PROJECT_SUCCESS;

  constructor(public payload: any) {
  }
}

export class WithdrawAllProjectFail implements Action {
  readonly type = WITHDRAW_ALL_PROJECT_FAIL;

  constructor(public payload: any) {
  }
}

export type AllProjectAction =
  | GetAllProjects
  | GetAllProjectsSuccess
  | GetAllProjectsFail
  | WithdrawAllProject
  | WithdrawAllProjectSuccess
  | WithdrawAllProjectFail;
