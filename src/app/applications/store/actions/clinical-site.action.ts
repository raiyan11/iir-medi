import {Action} from '@ngrx/store';
import {CREATE_BUDGET, CREATE_BUDGET_FAIL, CREATE_BUDGET_SUCCESS} from './budget.action';

export const CREATE_INVESTIGATOR = '[INVESTIGATOR] CREATE ';
export const CREATE_INVESTIGATOR_SUCCESS = '[INVESTIGATOR] CREATE Success';
export const CREATE_INVESTIGATOR_FAIL = '[INVESTIGATOR] CREATE Fail';

export const GET_INVESTIGATOR = '[INVESTIGATOR] GET ';
export const GET_INVESTIGATOR_SUCCESS = '[INVESTIGATOR] GET Success';
export const GET_INVESTIGATOR_FAIL = '[INVESTIGATOR] GET Fail';

export class CreateInvestigator implements Action {
  readonly type = CREATE_INVESTIGATOR;

  constructor(public payload: any) {
  }
}

export class CreateInvestigatorSuccess implements Action {
  readonly type = CREATE_INVESTIGATOR_SUCCESS;

  constructor(public payload: any) {
  }
}

export class CreateInvestigatorFail implements Action {
  readonly type = CREATE_INVESTIGATOR_FAIL;

  constructor(public payload: any) {
  }
}

export class GetInvestigator implements Action {
  readonly type = GET_INVESTIGATOR;

  constructor(public payload: any) {
  }
}

export class GetInvestigatorSuccess implements Action {
  readonly type = GET_INVESTIGATOR_SUCCESS;

  constructor(public payload: any) {
  }
}

export class GetInvestigatorFail implements Action {
  readonly type = GET_INVESTIGATOR_FAIL;

  constructor(public payload: any) {
  }
}

export type ClinicalSiteAction =
  CreateInvestigator
  | CreateInvestigatorSuccess
  | CreateInvestigatorFail
  | GetInvestigator
  | GetInvestigatorSuccess
  | GetInvestigatorFail;
