import {Injectable} from '@angular/core';
import {Actions, Effect} from '@ngrx/effects';
import * as addlInfoAction from '../actions/addl-info.action';
import {switchMap, map} from 'rxjs/operators';
import {of} from 'rxjs/observable/of';
import {Observable} from 'rxjs/Observable';
import {Action} from '@ngrx/store';
import {AddlInfoService} from '../../services/addl-info.service';

@Injectable()
export class AddlInfoEffect {
  constructor(private actions: Actions, private addlService: AddlInfoService) {
  }

  @Effect()
  createAdditionalInfo: Observable<Action> = this.actions.ofType(addlInfoAction.CREATE_ADDL_INFO)
    .pipe(
      map((action: addlInfoAction.CreateAddlInfo) => action.payload),
      switchMap(data => {
        const formData = new FormData();
        Object.keys(data.body).forEach(k => {
          if (!!data.body[k]) {
            formData.append(k, data.body[k]);
          }
        });
        return this.addlService.createAdditionalInfo(formData, data.projectId)
          .map(r => new addlInfoAction.CreateAddlInfoSuccess(r.documents))
          .catch(e => of(new addlInfoAction.CreateAddlInfoFail(e)));
      })
    );


}
