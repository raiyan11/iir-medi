import {Injectable} from '@angular/core';
import {Actions, Effect} from '@ngrx/effects';
import * as clinicalAction from '../actions/clinical-site.action';
import {switchMap, map} from 'rxjs/operators';
import {of} from 'rxjs/observable/of';
import {Observable} from 'rxjs/Observable';
import {Action} from '@ngrx/store';
import {ClinicalSiteService} from '../../services/clinical-site.service';

@Injectable()
export class ClinicalSiteEffect {
  constructor(private actions: Actions, private clinialService: ClinicalSiteService) {
  }

  @Effect()
  createInvestigator: Observable<Action> = this.actions.ofType(clinicalAction.CREATE_INVESTIGATOR)
    .pipe(
      map((action: clinicalAction.CreateInvestigator) => action.payload),
      switchMap(data => {
        // json => formData
        const formData = new FormData();
        Object.keys(data.body).forEach(key => {
          if (!!data.body[key]) {
            formData.append(key, data.body[key]);
          }
        });
        return this.clinialService.createInvestigators(formData, data.projectId)
          .map(r => new clinicalAction.CreateInvestigatorSuccess(r.investigator))
          .catch(e => of(new clinicalAction.CreateInvestigatorFail(e)));
      })
    );


}
