import {Injectable} from '@angular/core';
import {Actions, Effect} from '@ngrx/effects';
import {ProjectService} from '../../services/project.service';
import * as projectAction from '../actions/project.action';
import * as budgetAction from '../actions/budget.action';
import * as clinicalAction from '../actions/clinical-site.action';
import * as addlAction from '../actions/addl-info.action';
import * as allProjectsAction from '../actions/all-project.action';
import {switchMap, map} from 'rxjs/operators';
import {of} from 'rxjs/observable/of';
import {Observable} from 'rxjs/Observable';
import {Action} from '@ngrx/store';

@Injectable()
export class ProjectEffect {
  constructor(private actions: Actions, private projectService: ProjectService) {
  }

  @Effect()
  getAllProjects: Observable<Action> = this.actions.ofType(allProjectsAction.GET_ALL_PROJECTS)
    .pipe(
      switchMap(() => {
        return this.projectService.getAllApplications()
          .map(r => new allProjectsAction.GetAllProjectsSuccess(r.projects))
          .catch(e => of(new allProjectsAction.GetAllProjectsFail(e)));
      })
    );

  @Effect()
  createProject: Observable<Action> = this.actions.ofType(projectAction.CREATE_PROJECT)
    .pipe(
      map((action: projectAction.CreateProject) => action.payload),
      switchMap(body => {
        return this.projectService.createProject(body)
          .map(r => new projectAction.CreateProjectSuccess(r))
          .catch(e => of(new projectAction.CreateProjectFail(e)));
      })
    );

  @Effect()
  submitProject: Observable<Action> = this.actions.ofType(projectAction.SUBMIT_PROJECT)
    .pipe(
      map((action: projectAction.SubmitProject) => action.payload),
      switchMap(projectId => {
        return this.projectService.submitProject(projectId)
          .map(r => new projectAction.SubmitProjectSuccess())
          .catch(e => of(new projectAction.SubmitProjectFail(e)));
      })
    );

  @Effect()
  getProjectOverview: Observable<Action> = this.actions.ofType(projectAction.GET_PROJECT_OVERVIEW)
    .pipe(
      map((action: projectAction.GetProjectOverview) => action.payload),
      switchMap(projectId => {
        return this.projectService.getProjectOverview(projectId)
          .mergeMap(r => {
            const {budget, investigator, documents} = r;
            delete r.budget;
            delete r.investigator;
            delete r.documents;
            return [
              new budgetAction.GetBudgetSuccess(budget),
              new clinicalAction.GetInvestigatorSuccess(investigator),
              new addlAction.GetAddlInfoSuccess(documents),
              new projectAction.GetProjectOverviewSuccess(r)
            ];
          })
          .catch(e => of(new projectAction.GetProjectOverviewFail(e)));
      })
    );

  @Effect()
  updateProject: Observable<Action> = this.actions.ofType(projectAction.UPDATE_PROJECT)
    .pipe(
      map((action: projectAction.UpdateProject) => action.payload),
      switchMap(data => {
        return this.projectService.updateProject(data.body, data.projectId)
          .map(r => new projectAction.UpdateProjectSuccess(r))
          .catch(e => of(new projectAction.UpdateProjectFail(e)));
      })
    );

  @Effect()
  withdrawProject: Observable<Action> = this.actions.ofType(allProjectsAction.WITHDRAW_ALL_PROJECT)
    .pipe(
      map((action: projectAction.SubmitProject) => action.payload),
      switchMap(projectId => {
        return this.projectService.withdrawProject(projectId)
          .map(r => new allProjectsAction.WithdrawAllProjectSuccess(r))
          .catch(e => of(new allProjectsAction.WithdrawAllProjectFail(e)));
      })
    );
}
