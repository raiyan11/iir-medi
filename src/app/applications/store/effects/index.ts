import {ProjectEffect} from './project.effect';
import {ClinicalSiteEffect} from './clinical-site.effect';
import {BudgetEffect} from './budget.effect';
import {AddlInfoEffect} from './addl-info.effect';

export const ApplicationsEffects = [ProjectEffect, ClinicalSiteEffect, BudgetEffect, AddlInfoEffect];
