import {Injectable} from '@angular/core';
import {Actions, Effect} from '@ngrx/effects';
import * as budgetAction from '../actions/budget.action';
import {switchMap, map} from 'rxjs/operators';
import {of} from 'rxjs/observable/of';
import {Observable} from 'rxjs/Observable';
import {Action} from '@ngrx/store';
import {BudgetService} from '../../services/budget.service';

@Injectable()
export class BudgetEffect {
  constructor(private actions: Actions, private budgetService: BudgetService) {
  }

  @Effect()
  createBudget: Observable<Action> = this.actions.ofType(budgetAction.CREATE_BUDGET)
    .pipe(
      map((action: budgetAction.CreateBudget) => action.payload),
      switchMap(data => {
        const formData = new FormData();
        Object.keys(data.body).forEach(k => {
          if (!!data.body[k]) {
            formData.append(k, data.body[k]);
          }
        });
        return this.budgetService.createBudget(formData, data.projectId)
          .map(r => new budgetAction.CreateBudgetSuccess(r.budget))
          .catch(e => of(new budgetAction.CreateBudgetFail(e)));
      })
    );


}
