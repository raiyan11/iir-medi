import * as fromClinicalSite from '../actions/clinical-site.action';

export interface ClinicalSiteState {
  data: any;
  error: string;
  saving: boolean;
  saved: boolean;
  loading: boolean;
  loaded: boolean;
}

export const initialState = {
  data: null,
  error: null,
  saving: false,
  saved: false,
  loading: false,
  loaded: false,
};

export function reducer(state = initialState, action: fromClinicalSite.ClinicalSiteAction): ClinicalSiteState {
  switch (action.type) {
    case fromClinicalSite.CREATE_INVESTIGATOR: {
      return {
        ...state,
        saving: true,
        saved: false,
      };
    }
    case fromClinicalSite.CREATE_INVESTIGATOR_SUCCESS: {
      return {
        ...state,
        saving: false,
        saved: true,
        data: action.payload
      };
    }
    case fromClinicalSite.CREATE_INVESTIGATOR_FAIL: {
      return {
        ...state,
        saving: false,
        saved: false,
        error: action.payload
      };
    }

    case fromClinicalSite.GET_INVESTIGATOR: {
      return {
        ...state,
        loading: true,
        loaded: false,
      };
    }
    case fromClinicalSite.GET_INVESTIGATOR_SUCCESS: {
      return {
        ...state,
        loading: false,
        loaded: true,
        data: action.payload
      };
    }
    case fromClinicalSite.GET_INVESTIGATOR_FAIL: {
      return {
        ...state,
        loading: false,
        loaded: true,
        error: action.payload
      };
    }
  }
  return state;
}
