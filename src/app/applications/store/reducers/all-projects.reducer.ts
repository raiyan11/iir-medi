import * as fromAllProjects from '../actions/all-project.action';
import * as fromProject from '../actions/project.action';

export interface AllProjectsState {
  data: any;
  error: string;
  loading: boolean;
  loaded: boolean;
}

export const initialState = {
  data: null,
  error: null,
  loading: false,
  loaded: false,
};

export function reducer(state = initialState, action: fromAllProjects.AllProjectAction): AllProjectsState {
  switch (action.type) {
    case fromAllProjects.GET_ALL_PROJECTS: {
      return {
        ...state,
        loading: true,
        loaded: false,
      };
    }
    case fromAllProjects.GET_ALL_PROJECTS_SUCCESS: {
      return {
        ...state,
        loading: false,
        loaded: true,
        data: action.payload
      };
    }
    case fromAllProjects.GET_ALL_PROJECTS_FAIL: {
      return {
        ...state,
        loading: false,
        loaded: true,
        error: action.payload
      };
    }

    case fromAllProjects.WITHDRAW_ALL_PROJECT: {
      return {
        ...state,
        error: null,
      };
    }
    case fromAllProjects.WITHDRAW_ALL_PROJECT_SUCCESS: {
      const project = action.payload;
      const index = state.data.findIndex(p => p.id === project.id);
      state.data[index] = project;
      return {
        ...state,
      };
    }
    case fromAllProjects.WITHDRAW_ALL_PROJECT_FAIL: {
      return {
        ...state,
        error: action.payload
      };
    }
  }
  return state;
}
