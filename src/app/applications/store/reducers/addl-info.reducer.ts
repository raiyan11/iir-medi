import * as fromAddlInfo from '../actions/addl-info.action';

export interface AddlInfoState {
  data: any;
  error: string;
  saving: boolean;
  saved: boolean;
  loading: boolean;
  loaded: boolean;
}

export const initialState = {
  data: null,
  error: null,
  saving: false,
  saved: false,
  loading: false,
  loaded: false,
};

export function reducer(state = initialState, action: fromAddlInfo.AddlInfoAction): AddlInfoState {
  switch (action.type) {
    case fromAddlInfo.CREATE_ADDL_INFO: {
      return {
        ...state,
        saving: true,
        saved: false,
      };
    }
    case fromAddlInfo.CREATE_ADDL_INFO_SUCCESS: {
      return {
        ...state,
        saving: false,
        saved: true,
        data: action.payload
      };
    }
    case fromAddlInfo.CREATE_ADDL_INFO_FAIL: {
      return {
        ...state,
        saving: false,
        saved: false,
        error: action.payload
      };
    }

    case fromAddlInfo.GET_ADDL_INFO: {
      return {
        ...state,
        loading: true,
        loaded: false,
      };
    }
    case fromAddlInfo.GET_ADDL_INFO_SUCCESS: {
      return {
        ...state,
        loading: false,
        loaded: true,
        data: action.payload
      };
    }
    case fromAddlInfo.GET_ADDL_INFO_FAIL: {
      return {
        ...state,
        loading: false,
        loaded: false,
        error: action.payload
      };
    }
  }
  return state;
}
