import * as fromProject from './project.reducer';
import * as fromClinical from './clinical-site.reducer';
import * as fromBudget from './budget.reducer';
import * as fromAddlInfo from './addl-info.reducer';
import * as fromAllProjects from './all-projects.reducer';
import {ActionReducer, ActionReducerMap, combineReducers} from '@ngrx/store';

export interface ApplicationsState {
  all_projects: fromAllProjects.AllProjectsState,
  project: fromProject.ProjectState,
  clinical_site: fromClinical.ClinicalSiteState,
  budget: fromBudget.BudgetState,
  addl_info: fromAddlInfo.AddlInfoState
}

export const reducers: ActionReducerMap<ApplicationsState> = {
  all_projects: fromAllProjects.reducer,
  project: fromProject.reducer,
  clinical_site: fromClinical.reducer,
  budget: fromBudget.reducer,
  addl_info: fromAddlInfo.reducer,
};

export const reducer: ActionReducer<ApplicationsState> = combineReducers(reducers);
