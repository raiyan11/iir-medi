import * as fromProject from '../actions/project.action';

export interface ProjectState {
  data: any;
  error: string;
  saving: boolean;
  saved: boolean;
  loading: boolean;
  loaded: boolean;
}

export const initialState = {
  data: null,
  error: null,
  saving: false,
  saved: false,
  loading: false,
  loaded: false,
};

export function reducer(state = initialState, action: fromProject.ProjectAction): ProjectState {
  switch (action.type) {
    case fromProject.CREATE_PROJECT: {
      return {
        ...state,
        saving: true,
        saved: false,
      };
    }
    case fromProject.CREATE_PROJECT_SUCCESS: {
      return {
        ...state,
        saving: false,
        saved: true,
        data: action.payload
      };
    }
    case fromProject.CREATE_PROJECT_FAIL: {
      return {
        ...state,
        saving: false,
        saved: false,
        error: action.payload
      };
    }

    case fromProject.SUBMIT_PROJECT: {
      return {
        ...state,
        saving: true,
        saved: false,
        error: null,
      };
    }
    case fromProject.SUBMIT_PROJECT_SUCCESS: {
      return {
        ...state,
        saving: false,
        saved: true,
      };
    }
    case fromProject.SUBMIT_PROJECT_FAIL: {
      return {
        ...state,
        saving: false,
        saved: false,
        error: action.payload
      };
    }

    case fromProject.GET_PROJECT_OVERVIEW: {
      return {
        ...state,
        loading: true,
        loaded: false,
        error: null,
      };
    }
    case fromProject.GET_PROJECT_OVERVIEW_SUCCESS: {
      return {
        ...state,
        loading: false,
        loaded: true,
        data: action.payload,
      };
    }
    case fromProject.GET_PROJECT_OVERVIEW_FAIL: {
      return {
        ...state,
        loading: false,
        loaded: true,
        error: action.payload
      };
    }

    case fromProject.UPDATE_PROJECT: {
      return {
        ...state,
        saving: true,
        saved: false,
        error: null,
      };
    }
    case fromProject.UPDATE_PROJECT_SUCCESS: {
      return {
        ...state,
        saving: false,
        saved: true,
        data: action.payload,
      };
    }
    case fromProject.UPDATE_PROJECT_FAIL: {
      return {
        ...state,
        saving: false,
        saved: false,
        error: action.payload
      };
    }
  }
  return state;
}
