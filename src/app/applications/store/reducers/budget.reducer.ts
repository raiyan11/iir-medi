import * as fromBudget from '../actions/budget.action';

export interface BudgetState {
  data: any;
  error: string;
  saving: boolean;
  saved: boolean;
  loading: boolean;
  loaded: boolean;
}

export const initialState = {
  data: null,
  error: null,
  saving: false,
  saved: false,
  loading: false,
  loaded: false,
};

export function reducer(state = initialState, action: fromBudget.BudgetAction): BudgetState {
  switch (action.type) {
    case fromBudget.CREATE_BUDGET: {
      return {
        ...state,
        saving: true,
        saved: false,
      };
    }
    case fromBudget.CREATE_BUDGET_SUCCESS: {
      return {
        ...state,
        saving: false,
        saved: true,
        data: action['payload']
      };
    }
    case fromBudget.CREATE_BUDGET_FAIL: {
      return {
        ...state,
        saving: false,
        saved: false,
        error: action['payload']
      };
    }

    case fromBudget.GET_BUDGET: {
      return {
        ...state,
        loading: true,
        loaded: false,
      };
    }
    case fromBudget.GET_BUDGET_SUCCESS: {
      return {
        ...state,
        loading: false,
        loaded: true,
        data: action.payload
      };
    }
    case fromBudget.GET_BUDGET_FAIL: {
      return {
        ...state,
        loading: false,
        loaded: false,
        error: action.payload
      };
    }
  }
  return state;
}
