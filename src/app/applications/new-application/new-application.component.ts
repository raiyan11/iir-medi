import {Component, OnDestroy, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import * as fromStore from '../../store';

declare const $: any;

@Component({
  selector: 'app-new-application',
  templateUrl: './new-application.component.html',
  styleUrls: ['./new-application.component.scss'],
})
export class NewApplicationComponent implements OnInit,OnDestroy{
  currentTab = 0;

  constructor(private store:Store<fromStore.AppState>) {
  }

  ngOnDestroy(){
    this.store.dispatch(new fromStore.ResetProjectForms());
  }
  ngOnInit() {
    this.customFormConfig();
  }

  nextClick($event) {
    this.currentTab++;
    const $this = $event.target;
    const parent_fieldset = $($this).parents('fieldset');
    const next_step = true;
    // navigation steps / progress steps
    const current_active_step = $($this).parents('.f1').find('.f1-step.active');
    const progress_line = $($this).parents('.f1').find('.f1-progress-line');

    if (next_step) {
      parent_fieldset.fadeOut(400, () => {
        // change icons
        current_active_step.removeClass('active').addClass('activated').next().addClass('active');
        // progress bar
        this.bar_progress(progress_line, 'right');
        // show next step
        $($($this).parents()[1]).parent().parent().next().children().fadeIn();
        // scroll window to beginning of the form
        this.scroll_to_class($('.f1'), 20);
      });
    }
  }

  prevClick($event) {
    this.currentTab--;
    const $this = $event.currentTarget;
    const current_active_step = $($this).parents('.f1').find('.f1-step.active');
    const progress_line = $($this).parents('.f1').find('.f1-progress-line');

    $($this).parents('fieldset').fadeOut(400, () => {
      // change icons
      current_active_step.removeClass('active').prev().removeClass('activated').addClass('active');
      // progress bar
      this.bar_progress(progress_line, 'left');
      // show previous step
      $($($this).parents()[1]).parent().parent().prev().children().fadeIn();
      // scroll window to beginning of the form
      this.scroll_to_class($('.f1'), 20);
    });
  }


  scroll_to_class(element_class, removed_height) {
    const scroll_to = $(element_class).offset().top - removed_height;
    if ($(window).scrollTop() !== scroll_to) {
      $('html, body').stop().animate({scrollTop: scroll_to}, 0);
    }
  }

  bar_progress(progress_line_object, direction) {
    const number_of_steps = progress_line_object.data('number-of-steps');
    const now_value = progress_line_object.data('now-value');
    let new_value = 0;
    if (direction === 'right') {
      new_value = Math.floor(now_value + (100 / number_of_steps) + 4);
    }
    else if (direction === 'left') {
      new_value = Math.ceil(now_value - (100 / number_of_steps) - 4);
    }
    progress_line_object.attr('style', 'width: ' + new_value + '%;').data('now-value', new_value);
  }

  customFormConfig() {
    $('#top-navbar-1').on('shown.bs.collapse', function () {
      $.backstretch('resize');
    }).on('hidden.bs.collapse', function () {
      $.backstretch('resize');
    });

    // $('.f1 fieldset:first').fadeIn('slow');

    $('.f1 input[type="text"], .f1 input[type="password"], .f1 textarea').on('focus', function () {
      $(this).removeClass('input-error');
    });
  }
}
