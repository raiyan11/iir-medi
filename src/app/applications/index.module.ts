import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ApplicationsRoutingModule} from './applications-routing.module';
import {NewApplicationComponent} from './new-application/new-application.component';
import {AllApplicationsComponent} from './all-applications/all-applications.component';
import {ProjectFormComponent} from './project-form/project-form.component';
import {ClinicalSiteFormComponent} from './clinical-site-form/clinical-site-form.component';
import {BudgetFormComponent} from './budget-form/budget-form.component';
import {AdditionalInfoFormComponent} from './additional-info-form/additional-info-form.component';
import {PreviewFormComponent} from './preview-form/preview-form.component';
import {ConfirmationComponent} from './confirmation/confirmation.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ProjectService} from './services/project.service';
import {ClinicalSiteService} from './services/clinical-site.service';
import { ApplicationDetailComponent } from './application-detail/application-detail.component';
import { EditApplicationComponent } from './edit-application/edit-application.component';

@NgModule({
  imports: [
    CommonModule,
    ApplicationsRoutingModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  declarations: [
    NewApplicationComponent,
    AllApplicationsComponent,
    ProjectFormComponent,
    ClinicalSiteFormComponent,
    BudgetFormComponent,
    AdditionalInfoFormComponent,
    PreviewFormComponent,
    ConfirmationComponent,
    ApplicationDetailComponent,
    EditApplicationComponent
  ],
})
export class ApplicationsModule {
}
