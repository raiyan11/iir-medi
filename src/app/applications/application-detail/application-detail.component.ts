import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import * as fromStore from '../../store';
import {ToastrService} from 'ngx-toastr';
import {Store} from '@ngrx/store';
import {OtherEnrolmentGoalType, ProjectAreaDetail} from '../../models/project';
import {AddlInfoDocumentDetail} from '../../models/addl-info';

@Component({
  selector: 'app-application-detail',
  templateUrl: './application-detail.component.html',
  styleUrls: ['./application-detail.component.scss']
})
export class ApplicationDetailComponent implements OnInit {
  application;
  projectAreaDetail = ProjectAreaDetail;

  constructor(private route: ActivatedRoute,
              private store: Store<fromStore.AppState>,
              private toastr: ToastrService) {
  }

  ngOnInit() {
    this.store.dispatch(new fromStore.GetProjectOverview(this.route.snapshot.params.applicationId));
    this.application = this.store.select(s => s.applications);
  }

  getProjectAreaDetail(area) {
    if (area === null || area == undefined) {
      return '-';
    }
    return this.projectAreaDetail[`${area.toUpperCase()}`];
  }

  addlInfoDocumentTitle(category) {
    if (category === null || category == undefined) {
      return '-';
    }
    return AddlInfoDocumentDetail[category];
  }

  getProjectOtherEnrollmentGoal(goal) {
    if (goal === null || goal == undefined) {
      return '-';
    }
    const goalTypesValues = ['1 - 10', '10 - 50', '50 - 100', '100 - 300', '300+'];
    const r = Object.values(OtherEnrolmentGoalType).findIndex(g => +g === +goal);
    return goalTypesValues[r];
  }
}
