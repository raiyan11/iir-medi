import {Component, EventEmitter, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import {Budget, TotalRequestType} from '../../models/budget';
import {Store} from '@ngrx/store';
import * as fromStore from '../../store';
import {Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import {Project} from '../../models/project';
import {take} from 'rxjs/operators';
import {isNullOrUndefined} from 'util';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-budget-form',
  templateUrl: './budget-form.component.html',
  styleUrls: ['./budget-form.component.scss']
})
export class BudgetFormComponent implements OnInit, OnDestroy {
  @Output() onNextClick = new EventEmitter<any>();
  @Output() onPrevClick = new EventEmitter<any>();
  @ViewChild('budgetForm') budgetForm: NgForm;
  budget = new Budget();
  budgetRequestTypes;
  project: Project;
  projectStore;
  budgetStore;
  submitted;

  constructor(private store: Store<fromStore.AppState>,
              private router: Router,
              private toastr: ToastrService) {
  }

  ngOnInit() {
    this.submitted = false;
    this.projectStore = this.store.select(s => s.applications.project).subscribe(project => {
      this.project = project.data;
    });
    this.store.select(s => s.applications.budget).pipe(take(2)).subscribe(r => {
      // or saved as draft.
      if ((r.saved || r.loaded) && (!isNullOrUndefined(r.data))) {
        this.budget = r.data;
      }
    });
    this.budgetRequestTypes = Object.values(TotalRequestType).map(value => {
      return {key: value, value: value.split('_').join(' ')};
    });
  }

  ngOnDestroy() {
    if (this.budgetStore) {
      this.budgetStore.unsubscribe();
    }
    this.projectStore.unsubscribe();
  }

  onDocumentChange($event) {
    if ($event.target.files && $event.target.files[0]) {
      this.budget.file = $event.target.files[0];
      this.budget.documents = [];
    }
  }

  saveDraft() {
    this.budget.draft = true;
    this.storeDispatch();
  }

  formSubmit($event) {
    this.budget.draft = false;
    this.storeDispatch($event);
  }

  storeDispatch($event = null) {
    this.submitted = true;
    this.store.dispatch(new fromStore.CreateBudget({body: this.budget, projectId: this.project.id}));
    this.store.select(s => s.applications.budget).pipe(take(2)).subscribe(r => {
      this.toastr.clear();
      if (r.error) {
        this.submitted = false;
        this.toastr.error(r.error, 'Error');
        return;
      }
      if (r.saving) {
        this.toastr.info('Saving...');
      }
      if (r.saved) {
        if ($event) {
          this.toastr.success('Saved');
          this.onNextClick.emit($event);
        } else {
          this.submitted = false;
          this.toastr.success('Saved as Draft');
        }
      }
    });
  }

  prevClick($event) {
    this.onPrevClick.emit($event);
  }

}
