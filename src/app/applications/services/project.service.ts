import {Injectable} from '@angular/core';
import {environment} from '../../../environments/environment';
import {Observable} from 'rxjs/Observable';
import {HttpClient} from '@angular/common/http';
import {AppConstants} from '../../shared/app.constants';

@Injectable()
export class ProjectService {

  constructor(private http: HttpClient, private appConstants: AppConstants) {
  }

  getAllApplications() {
    const url = environment.apiUrl + '/projects/list';
    const headers = this.appConstants.JSONHeaders();
    return this.http.get(url, {headers: headers})
      .map(r => r)
      .catch(this.appConstants.errorHandler);
  }

  createProject(body): Observable<any> {
    const url = environment.apiUrl + '/projects/create';
    return this.http.post(url, body, {headers: this.appConstants.JSONHeaders()})
      .map(r => r)
      .catch(this.appConstants.errorHandler);
  }

  submitProject(projectId): Observable<any> {
    const url = environment.apiUrl + `/projects/${projectId}/submit`;
    return this.http.put(url, {}, {headers: this.appConstants.JSONHeaders()})
      .map(r => r)
      .catch(this.appConstants.errorHandler);
  }

  getProjectOverview(projectId): Observable<any> {
    const url = environment.apiUrl + `/projects/${projectId}`;
    const headers = this.appConstants.JSONHeaders();
    return this.http.get(url, {headers: headers}).catch(this.appConstants.errorHandler);
  }

  updateProject(body, projectId): Observable<any> {
    const url = environment.apiUrl + `/projects/${projectId}`;
    return this.http.put(url, body, {headers: this.appConstants.JSONHeaders()})
      .map(r => r)
      .catch(this.appConstants.errorHandler).catch(this.appConstants.errorHandler);
  }

  withdrawProject(projectId): Observable<any> {
    const url = environment.apiUrl + `/projects/${projectId}/withdraw`;
    return this.http.put(url, {}, {headers: this.appConstants.JSONHeaders()})
      .map(r => r)
      .catch(this.appConstants.errorHandler);
  }
}
