import { TestBed, inject } from '@angular/core/testing';

import { ClinicalSiteService } from './clinical-site.service';

describe('ClinicalSiteService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ClinicalSiteService]
    });
  });

  it('should be created', inject([ClinicalSiteService], (service: ClinicalSiteService) => {
    expect(service).toBeTruthy();
  }));
});
