import {Injectable} from '@angular/core';
import {environment} from '../../../environments/environment';
import {Observable} from 'rxjs/Observable';
import {HttpClient} from '@angular/common/http';
import {AppConstants} from '../../shared/app.constants';

@Injectable()
export class AddlInfoService {

  constructor(private http: HttpClient, private appConstants: AppConstants) {
  }

  createAdditionalInfo(body, projectId): Observable<any> {
    const url = environment.apiUrl + `/projects/${projectId}/addl_info`;
    let headers = this.appConstants.JSONHeaders();
    headers = headers.delete('Content-Type');
    return this.http.put(url, body, {headers: headers})
      .map(r => r)
      .catch(this.appConstants.errorHandler);
  }
}
