import { TestBed, inject } from '@angular/core/testing';

import { AddlInfoService } from './addl-info.service';

describe('AddlInfoService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AddlInfoService]
    });
  });

  it('should be created', inject([AddlInfoService], (service: AddlInfoService) => {
    expect(service).toBeTruthy();
  }));
});
