export class Note {
  constructor(public id?: number,
              public notes?: string,
              public projectId?: number,
              public createdAt?: string,
              public updatedAt?: string) {
  }
}
