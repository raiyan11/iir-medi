import {Document} from './document';

export class Budget {
  constructor(public id?: number,
              public amount?: number,
              public timeFrame?: string,
              public totalRequest?: TotalRequestType,
              public file?: any,
              public draft: boolean = false,
              public documents?: Document[],
              public projectId?: number) {
  }
}

export enum TotalRequestType {
  DIRECT_FUNDING = 'direct_funding',
  DEVICES = 'devices',
  OTHER_SUPPORT = 'other_support'

}
