import {Document} from './document';

export class AddlInfo {
  constructor(public study_protocol?: any,
              public consent_form?: any,
              public funding?: any,
              public cv_others?: any,
              public certification?: any,
              public cover_letter?: any,
              public draft: boolean = false,
              public documents?: Document[]) {
  }

}


export enum AddlInfoDocumentTypes {
  STUDY_PROTOCOL = 'study_protocol',
  CONSENT_FORM = 'consent_form',
  FUNDING = 'funding',
  CV_OTHERS = 'cv_others',
  CERTIFICATION = 'certification',
  COVER_LETTER = 'cover_letter'
}

export enum AddlInfoDocumentDetail {
  study_protocol = 'DETAILED STUDY PROTOCOL',
  consent_form = 'DRAFT INFORMED CONSENT FORM',
  funding = 'TOTAL FUNDING',
  cv_others = 'CV',
  certification = 'CERTIFICATION',
  cover_letter = 'COVER LETTER'

}
