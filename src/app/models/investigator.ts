import {Document} from './document';

export class Investigator {

  constructor(public id?: number,
              public name?: string,
              public institution?: string,
              public phone?: string,
              public email?: string,
              public address?: string,
              public otherContact?: string,
              public projectId?: number,
              public createdAt?: string,
              public file?: any,
              public draft: boolean = false,
              public documents?: Document[],
              public updatedAt?: string) {
  }

}
