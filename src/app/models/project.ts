import {Document} from './document';

export class Project {
  constructor(public id?: number,
              public userId?: number,
              public status?: ProjectStatusType,
              public stage?: ProjectStageType,
              public applicationId?: string,
              public title?: string,
              public category?: ProjectCategoryType,
              public objectives?: string,
              public studyDates?: string,
              public hypothesis?: string,
              public intervention?: string,
              public outcomesMeasured?: string,
              public internalContact?: string,
              public phase?: number,
              public patientPopulation?: PatientPopulationType,
              public otherEnrollmentGoal?: OtherEnrolmentGoalType,
              public area?: string,
              public createdAt?: string,
              public updatedAt?: string,
              public submittedAt?: string,
              public draft: boolean = false,
  ) {
  }
}

export enum ProjectStatusType {
  IN_PROGRESS = 'in-progress',
  SUBMITTED = 'submitted',
  APPROVED = 'approved',
  REJECTED = 'rejected',
  DRAFT = 'draft'
}

export enum ProjectStageType {
  description,
  site_info,
  budget,
  addl_info,
  preview,
  submitted
}

export enum ProjectCategoryType {
  CONCEPT = 'concept',
  FULL_PROPOSAL = 'full_proposal'
}

export enum PatientPopulationType {
  COPD = 'copd',
  OSA = 'osa',
  CSA = 'csa',
  COPD_OSA = 'copd_osa',
  NEUROM = 'neurom',
  HEART_FAILURE = 'heart_failure',
  OTHER_DIsEASE = 'other_disease'
}

export enum OtherEnrolmentGoalType {
  ONE = '1',
  TEN = '10',
  FIFTY = '50',
  HUNDRED = '100',
  THREE_HUNDERD = '300'
}


export enum ProjectAreaType {
  CLINICAL_PATHWAY_OPT = 'clinical_pathway_opt',
  COMPLIANCE = 'compliance',
  ECONOMIC_VALUE = 'economic_value',
  CLINICAL_COUTCOME = 'clinical_coutcome',
  SCIENTIFIC_KNOWLEDGE = 'scientific_knowledge',
  NEW_INSIGHTS = 'new_insights'
}

export enum ProjectAreaDetail {
  CLINICAL_PATHWAY_OPT = 'Clinical pathway optimization in diagnosis ans treatment of sleep apnea',
  COMPLIANCE = 'Compliance and long-term adherence to PAP ther apy through telemedicine',
  ECONOMIC_VALUE = 'Economic value of treating sleep apnea and COPD',
  CLINICAL_COUTCOME = 'Clinical outcomes and quality of life with the use of portable oxygen concentrators',
  SCIENTIFIC_KNOWLEDGE = 'Scientific knowledge and patient pathways for the use of NIV',
  NEW_INSIGHTS = 'New insights into utilizing clinical data to improve patient’s lives'
}


export enum PatientPopulationTypeDetail {
  copd = 'COPD',
  osa = 'OSA',
  csa = 'CSA',
  copd_osa = 'COPD/OSA overlap',
  neurom = 'Neurom uscular disease',
  heart_failure = 'Heart Failure',
  other_disease = 'Other Disease'
}
