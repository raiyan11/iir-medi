import {s} from '@angular/core/src/render3';

export class Document {
  constructor(public id?: number,
              public category?: DocumentCategoryType,
              public documentableId?: number,
              public documentable?: string,
              public createdAt?: string,
              public file?: File,
              public updatedAt?: string) {
  }

}

export class File {
  constructor(public acl?: string,
              public bucket?: string,
              public contentDisposition?: string,
              public contentType?: string,
              public location?: string,
              public mimetype?: string,
              public originalname?: string) {
  }
}

export enum DocumentCategoryType {
  CV = 'cv',
  BUDGET = 'budget',
  STUDY_PROTOCOL = 'study_protocol',
  CONSENT_FORM = 'consent_form',
  FUNDING = 'funding',
  CV_OTHERS = 'cv_others',
  CERTIFICATION = 'certification',
  COVER_LETTER = 'cover_letter'
}
