export class User {
  constructor(public id?: number,
              public firstName?: string,
              public lastName?: string,
              public email?: string,
              public userId?: number,
              public createdAt?: string,
              public updatedAt?: string) {
  }
}
