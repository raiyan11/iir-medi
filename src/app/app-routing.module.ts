import {APP_INITIALIZER, NgModule} from '@angular/core';
import {PreloadAllModules, RouterModule, Routes} from '@angular/router';
import {OktaAuthService} from './shared/services/okta-auth.service';
import {AuthService} from './shared/services/auth.service';
import {UserLoadInjector} from './shared/user.load.injector';
import {Store} from '@ngrx/store';
import {ErrorResponseInterceptor} from './shared/error.response.interceptor';
import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {AuthGuard} from './auth-guard.service';
import {LoginGuard} from './login-guard.service';

const routes: Routes = [
  {path: '', redirectTo: 'home', pathMatch: 'full'},
  {path: 'home', loadChildren: 'app/home/index.module#HomeModule'},
  {path: 'implicit/callback', loadChildren: 'app/okta-callback/index.module#OktaCallbackModule'},
  {path: '', loadChildren: 'app/dashboard/index.module#DashboardModule', canActivate: [AuthGuard]},
  {path: 'login', loadChildren: 'app/login/index.module#LoginModule', canActivate: [LoginGuard]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {preloadingStrategy: PreloadAllModules, initialNavigation: 'enabled'})],
  exports: [RouterModule],
  providers: [
    OktaAuthService,
    AuthService,
    AuthGuard,
    LoginGuard,
    {provide: APP_INITIALIZER, useFactory: UserLoadInjector, multi: true, deps: [Store]},
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ErrorResponseInterceptor,
      multi: true,
    }
  ]
})
export class AppRoutingModule {
}

// TODO: Add LoginGuard & AuthGuard
