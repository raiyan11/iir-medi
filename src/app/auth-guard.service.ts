import {NgModule} from '@angular/core';
import {Router} from '@angular/router';
import {CanActivate} from '@angular/router';
import {Store} from '@ngrx/store';
import * as fromStore from './store';
import {OktaAuthService} from './shared/services/okta-auth.service';

@NgModule({
  providers: []
})

export class AuthGuard implements CanActivate {

  constructor(private oktaAuth: OktaAuthService, private router: Router, private store: Store<fromStore.AppState>) {
  }

  async canActivate() {
    if (await this.oktaAuth.isAuthenticated()) {
      return true;
    }
    this.router.navigate(['/login']);
    return false;
  }
}
