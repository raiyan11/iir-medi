import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {OktaCallbackComponent} from './okta-callback.component';

const routes: Routes = [
  {path: '', component: OktaCallbackComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OktaCallbackRoutingModule {
}
