import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {OktaCallbackRoutingModule} from './okta-callback-routing.module';
import {OktaCallbackComponent} from './okta-callback.component';

@NgModule({
  imports: [
    CommonModule,
    OktaCallbackRoutingModule
  ],
  declarations: [OktaCallbackComponent]
})
export class OktaCallbackModule {
}
