import {Component, OnInit} from '@angular/core';
import {OktaAuthService} from '../shared/services/okta-auth.service';
import {Router} from '@angular/router';
import {Store} from '@ngrx/store';
import * as fromStore from '../store';
import {take} from 'rxjs/operators';


@Component({
  selector: 'app-okta-callback',
  templateUrl: './okta-callback.component.html',
  styleUrls: ['./okta-callback.component.scss']
})
export class OktaCallbackComponent implements OnInit {

  constructor(private oktaAuth: OktaAuthService,
              private router: Router,
              private store: Store<fromStore.AppState>) {
  }

  async ngOnInit() {
    const session = await this.oktaAuth.getSession();
    if (session.status === 'ACTIVE') {
      this.store.dispatch(new fromStore.OktaLogin());
      this.store.select(s => s.auth).pipe(take(2)).subscribe(r => {
        if (r.loggedOut) {
          return;
        }
        if (r.loggedIn || r.isOktaUser) {
          this.router.navigate(['/applications']);
        } else if (r.error) {
          this.router.navigate(['/login']);
        }
      });
    } else {
      this.store.dispatch(new fromStore.OktaLoginFail('User not authenticated. Try Again!'));
      this.router.navigate(['/login']);
    }
    // if (await this.oktaAuth.getSession().status === 'ACTIVE') {
    //   this.store.dispatch(new fromStore.OktaLogin());
    // } else {
    //   this.router.navigate(['/login']);
    // }
    // this.oktaAuth.handleAuthentication().then((tokens) => {
    //   if (tokens) {
    //     this.store.dispatch(new fromStore.OktaLogin());

  }

}
