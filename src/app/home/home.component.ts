import {Component, OnInit} from '@angular/core';
import {OktaAuthService} from '../shared/services/okta-auth.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  _loggedIn;

  constructor(private oktaAuth: OktaAuthService) {
  }

  async ngOnInit() {
    this._loggedIn = await this.oktaAuth.isAuthenticated();
  }

}
