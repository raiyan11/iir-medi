import {NgModule} from '@angular/core';
import {Router} from '@angular/router';
import {CanActivate} from '@angular/router';
import {Store} from '@ngrx/store';
import * as fromStore from './store';
import {OktaAuthService} from './shared/services/okta-auth.service';

@NgModule({
  providers: []
})

export class LoginGuard implements CanActivate {

  constructor(private oktaAuth: OktaAuthService, private router: Router, private store: Store<fromStore.AppState>) {
  }

  async canActivate() {
    if (await this.oktaAuth.isAuthenticated()) {
      this.router.navigate(['/applications']);
      return false;
    }
    return true;
  }
}
