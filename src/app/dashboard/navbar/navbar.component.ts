import {Component, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import * as fromStore from '../../store';
import {Observable} from 'rxjs/Observable';

declare const $: any;

@Component({
  selector: 'app-dashboard-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  navBarOpen = false;
  profileOpen = false;
  loggedIn$: Observable<any>;
  loggedOut$: Observable<any>;
  currentUser: Observable<any>;

  constructor(private store: Store<fromStore.AppState>) {
  }

  ngOnInit() {
    $('#router__page_content').on('click', () => {
      if (this.profileOpen) {
        this.profileOpen = false;
      }
    });
    this.loggedIn$ = this.store.select(s => s.auth.loggedIn);
    this.loggedOut$ = this.store.select(s => s.auth.loggedOut);
    this.currentUser = this.store.select(s => s.auth.user);

  }

  toggleNavbar() {
    this.navBarOpen = !this.navBarOpen;
  }

  toggleProfileOptions() {
    this.profileOpen = !this.profileOpen;
  }

  logoutAction() {
    if (confirm('Do you want to logout?')) {
      this.profileOpen = false;
      this.store.dispatch(new fromStore.LogoutUser());
    }
  }
}
