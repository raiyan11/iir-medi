export const environment = {
  production: true,
  version: require('../../package.json').version,
  loginUrl: 'http://localhost:3000/login',
  apiUrl: 'http://localhost:3000',
  oktaConfig: {
    url: 'https://dev-495539.oktapreview.com',
    issuer: 'https://dev-495539.oktapreview.com/oauth2/default',
    redirectUri: 'http://localhost:4201/implicit/callback',
    clientId: '0oafo2rehr4ARH7kz0h7',
    scope: 'openid profile email'
  }
};
