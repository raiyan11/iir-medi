// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  version: '0.0.1',
  loginUrl: 'http://localhost:3000/login',
  apiUrl: 'http://localhost:3000',
  oktaConfig: {
    url: 'https://dev-495539.oktapreview.com',
    issuer: 'https://dev-495539.oktapreview.com/oauth2/default',
    redirectUri: 'http://localhost:4201/implicit/callback',
    clientId: '0oafo2rehr4ARH7kz0h7',
    scope: 'openid profile email',
    responseType: ['id_token', 'token'],
    display: 'page'
  }
};
