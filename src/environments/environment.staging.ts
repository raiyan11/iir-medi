export const environment = {
  production: true,
  version: require('../../package.json').version,
  loginUrl: 'https://backend.iir.bitcot.com/login',
  apiUrl: 'https://backend.iir.bitcot.com',
  oktaConfig: {
    url: 'https://dev-495539.oktapreview.com',
    issuer: 'https://dev-495539.oktapreview.com/oauth2/default',
    redirectUri: 'https://iir.bitcot.com/implicit/callback',
    clientId: '0oafo2rehr4ARH7kz0h7',
    scope: 'openid profile email'
  }
};
